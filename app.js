const {app, BrowserWindow} = require('electron');
const exec = require('child_process').spawn;

let mainWindow;

function createWindow() {
    mainWindow = new BrowserWindow({width: 1920, height: 1080, fullscreen: true});

    mainWindow.loadFile('temp.html');

    mainWindow.on('closed', () => {
        mainWindow = null;
    })
}
app.on('ready', createWindow);
